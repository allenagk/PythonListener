import paho.mqtt.client as mqtt
import datetime
import pymongo
from pymongo import MongoClient
#from bson.objectid import ObjectId
#from db import save_to_db

client = MongoClient()

db = client.virtusa
collection = db.info


# The callback for when the client receives a CONNACK response from the server.
def on_connect(mqttc, userdata, rc):
    print('connected...rc=' + str(rc))
    mqttc.subscribe(topic="test/t1", qos=0)

#def on_disconnect(mqttc, userdata, rc):
#    print('disconnected...rc=' + str(rc))


# The callback for when a PUBLISH message is received from the server.
def on_message(mqttc, userdata, msg):
    #print('message received...')
	post = {"date": datetime.datetime.utcnow(),
	      "details":msg.topic,
		  "message":msg.payload}
	db.info.insert_one(post).inserted_id
    #print 'corresponding db entry==>%s'	% (db.info.find_one({"message":msg.payload}))
	
	
	

#def on_subscribe(mqttc, userdata, mid, granted_qos):
 #   print('subscribed (qos=' + str(granted_qos) + ')')

#def on_unsubscribe(mqttc, userdata, mid, granted_qos):
#    print('unsubscribed (qos=' + str(granted_qos) + ')')

mqttc = mqtt.Client()
mqttc.on_connect = on_connect
#mqttc.on_disconnect = on_disconnect
mqttc.on_message = on_message
#mqttc.on_subscribe = on_subscribe
#mqttc.on_unsubscribe = on_unsubscribe

aws="ec2-54-68-239-76.us-west-2.compute.amazonaws.com"
mqttc.connect(host=aws, port=1883)
mqttc.loop_forever()
